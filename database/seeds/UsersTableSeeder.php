<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create([
            'name' => 'Diego Gonzalez',
            'email' => 'gonzalezdiego1990@gmail.com',
            'password' => bcrypt('secret'),
        ]);
        factory(App\User::class, 29)->create();
    }
}
